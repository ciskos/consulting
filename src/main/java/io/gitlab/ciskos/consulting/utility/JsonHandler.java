package io.gitlab.ciskos.consulting.utility;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.gitlab.ciskos.consulting.dto.WordDTO;

public class JsonHandler {

	private JsonHandler() {
	}
	
	public static String bindingResultToJson(BindingResult bindingResult) throws JsonProcessingException {
		Map<String, String> collect = bindingResult.getFieldErrors().stream()
				.collect(Collectors.toMap(FieldError::getField
						, FieldError::getDefaultMessage
						, (oldValue, newValue) -> oldValue + ", " + newValue));
		
		return objectToJson(collect);
	}

	public static String objectToJson(Object input) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.registerModule(new JavaTimeModule());
		
		return mapper.writeValueAsString(input);
	}	
	
	public static String dtoToJson(WordDTO wordDTO) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(stringToMap(wordDTO.getWord()));
	}

	public static Map<String, Long> stringToMap(String word) {
		return word.chars()
				.mapToObj(c -> Character.toString((char) c))
		        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
		        .entrySet().stream()
		        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
		        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}
	
}
