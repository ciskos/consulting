package io.gitlab.ciskos.consulting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class ConsultingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultingApplication.class, args);
	}

}
