package io.gitlab.ciskos.consulting.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WordDTO {

	@NotBlank(message = "Word must not be blank.")
	private String word;
	
}
