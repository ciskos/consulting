package io.gitlab.ciskos.consulting.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.gitlab.ciskos.consulting.dto.WordDTO;
import io.gitlab.ciskos.consulting.utility.JsonHandler;

@RestController
@RequestMapping(value = RootController.ROOT_PATH)
public class RootController {

	private static final String GET_ANSWER = "Create Post request to get answer.";
	private static final String ROOT_PATH = "/";

	@GetMapping("/")
	public ResponseEntity<String> getRoot() throws JsonProcessingException  {
		return new ResponseEntity<>(JsonHandler.objectToJson(GET_ANSWER), HttpStatus.OK);
	}
	
	@PostMapping(name = "/", produces = "application/json")
	public ResponseEntity<String> getRoot(
			@Valid @RequestBody WordDTO wordDTO
			, BindingResult bindingResult) throws JsonProcessingException {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(JsonHandler.bindingResultToJson(bindingResult), HttpStatus.BAD_REQUEST);
		}
		
		
		return ResponseEntity.ok(JsonHandler.dtoToJson(wordDTO));
	}

}
