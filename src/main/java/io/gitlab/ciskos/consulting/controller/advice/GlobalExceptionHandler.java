package io.gitlab.ciskos.consulting.controller.advice;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.gitlab.ciskos.consulting.exception.ErrorMessage;
import io.gitlab.ciskos.consulting.utility.JsonHandler;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> defaultErrorHandler(WebRequest request, Exception e) throws JsonProcessingException {
		ErrorMessage message = new ErrorMessage(
					HttpStatus.BAD_REQUEST.value()
					, LocalDate.now()
					, e.getMessage()
					, request.getDescription(false));
		
		return new ResponseEntity<String>(JsonHandler.objectToJson(message), HttpStatus.BAD_REQUEST);
	}
	
}
