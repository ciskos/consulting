package io.gitlab.ciskos.consulting.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = RootController.class)
class RootControllerTest {

	@Autowired
	protected MockMvc mockMvc;
	
	@Test
	@DisplayName("Test GET")
	void testGetRootGet() throws Exception {
		String result = "Create Post request to get answer.";
		
		mockMvc.perform(get("/"))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

	@Test
	@DisplayName("Test POST")
	void testGetRootPost() throws Exception {
		String content = "{\"word\": \"aaaaabcccc\"}";
		String result = "{\"a\":5,\"c\":4,\"b\":1}";
		
		mockMvc.perform(post("/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(content))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(content().string(containsString(result)));
	}

}
