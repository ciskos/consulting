Тестовое задание.

Спроектировать(продумать формат и ограничения входящих/исходящих параметров) и реализовать REST API, вычисляющее частоту встречи символов по заданной строке. Результат должен быть отсортирован по убыванию количества вхождений символа в заданную строку.

Пример входной строки: “aaaaabcccc”
Пример выходного результата: “a”: 5, “c”: 4, “b”: 1

Требования к решению:
1. Java 8+
2. Spring boot 2+
3. Решение должно быть покрыто тестами
4. У решения должна быть документация по запуску и формату входящих/исходящих параметров
5. Код решения необходимо разместить в публичном Github репозитории.

===

API Документация - http://localhost:8080/swagger-ui/index.html

===

cUrl

GET

curl --location 'localhost:8080/' --header 'Accept: application/json'

"Create Post request to get answer."

---

POST

curl --location 'localhost:8080/' --header 'Content-Type: application/json' --data '{"word":"woorrrdd"}'

{
    "r": 3,
    "d": 2,
    "o": 2,
    "w": 1
}

---

POST - blank word

curl --location 'localhost:8080/' --header 'Content-Type: application/json' --data '{}'

{
    "word": "Word must not be blank."
}

---

ERROR HANDLING

curl --location --request POST 'localhost:8080/' --header 'Content-Type: application/json' --data ''

{"statusCode":400,"timestamp":[2023,9,29],"message":"Required request body is missing: public org.springframework.http.ResponseEntity<java.lang.String> io.gitlab.ciskos.consulting.controller.RootController.getRoot(io.gitlab.ciskos.consulting.dto.WordDTO,org.springframework.validation.BindingResult) throws com.fasterxml.jackson.core.JsonProcessingException","description":"uri=/"}